#version 400
in vec4 vPosition;
in vec4 vColor;
out vec4 color;

uniform mat4 Model;
uniform mat4 Projection;

void main()
{
    gl_Position = Projection * Model * vPosition / vPosition.w;
	color=vColor;
}//